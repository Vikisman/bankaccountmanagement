package config;

import dto.CardDTO;

public class CardSession {
    private CardDTO cardSessionDTO;

    private static CardSession instance;

    private CardSession(){}

    public static CardSession getInstance() {
        if(instance == null) {
            instance = new CardSession();
        }
        return instance;
    }
    public CardDTO getCardSessionDTO () {return cardSessionDTO;}
    public void setCardSessionDTO (CardDTO cardSessionDTO) {this.cardSessionDTO = cardSessionDTO;}
}
