package dto;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Optional;

public class CardDTO {
    private Integer cardId;
    private Integer customerId;
    private String cardholderName;
    private Integer cardExpDate;
    private Integer cvv;
    private Integer balance;
    private Integer pin;

    public CardDTO(){}

    public CardDTO(Integer customerId, Integer cardId, String cardholderName, Integer cardExpDate, Integer cvv, Integer balance, Integer pin){
        this.cardId = cardId;
        this.customerId = customerId;
        this.cardholderName = cardholderName;
        this.cardExpDate = cardExpDate;
        this.cvv = cvv;
        this.balance = balance;
        this.pin = pin;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public Integer getCardExpDate() {
        return cardExpDate;
    }

    public void setCardExpDate(Integer cardExpDate) {
        this.cardExpDate = cardExpDate;
    }

    public Integer getCvv() {
        return cvv;
    }

    public void setCvv(Integer cvv) {
        this.cvv = cvv;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    @Override
    public String toString() {
        return "CardDTO{" +
                "cardId=" + cardId +
                ", customerId=" + customerId +
                ", cardholderName='" + cardholderName + '\'' +
                ", cardExpDate=" + cardExpDate +
                ", cvv=" + cvv +
                ", balance=" + balance +
                ", pin=" + pin +
                '}';
    }
}
