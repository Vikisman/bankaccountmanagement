package dto;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {

    private Integer customerId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private List<UserCard> userCards = new ArrayList<>(0);

    public UserDTO(){}

    public UserDTO(Integer customerId, String username, String password, String firstName, String lastName) {
        this.customerId = customerId;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        customerId = customerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<UserCard> getUserCards() {
        return userCards;
    }

    public void setUserCards(List<UserCard> userCards) {
        this.userCards = userCards;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "CustomerId=" + customerId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userCards=" + userCards +
                '}';
    }
}


