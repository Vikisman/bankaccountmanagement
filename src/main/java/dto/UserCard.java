package dto;

public class UserCard {
    private String cardholderName;
    private Integer cardId;
    private Integer balance;

    public UserCard() {}

    public UserCard(String cardholderName, Integer cardId, Integer balance) {
        this.cardholderName = cardholderName;
        this.cardId = cardId;
        this.balance = balance;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "UserCard{" +
                "cardholderName='" + cardholderName + '\'' +
                ", cardId=" + cardId +
                ", balance=" + balance +
                '}';
    }
}
