package mapper;

import dto.CardDTO;
import entities.Card;

public class CardMapper {
    public static CardDTO mapEntityToDto(Card entityCard) {
        if (null == entityCard) {
            return null;
        }
        CardDTO cardDTO = new CardDTO();

        cardDTO.setCardId(entityCard.getCardId());
        cardDTO.setCustomerId(entityCard.getCustomerId());
        cardDTO.setCardholderName(entityCard.getCardholderName());
        cardDTO.setCardExpDate(entityCard.getCardExpDate());
        cardDTO.setCvv(entityCard.getCvv());
        cardDTO.setBalance(entityCard.getBalance());
        cardDTO.setPin(entityCard.getPin());

        return cardDTO;
    }



    public static Card mapDtoToEntity(CardDTO cardDTO) {
        if (null == cardDTO) {
            return null;
        }
        Card cardEntity = new Card();

        cardEntity.setCardId(cardDTO.getCardId());
        cardEntity.setCustomerId(cardDTO.getCustomerId());
        cardEntity.setCardholderName(cardDTO.getCardholderName());
        cardEntity.setCardExpDate(cardEntity.getCardExpDate());
        cardEntity.setCvv(cardEntity.getCvv());
        cardEntity.setBalance(cardDTO.getBalance());
        cardEntity.setPin(cardDTO.getPin());

        return cardEntity;
    }
}
