package mapper;

import dto.UserDTO;
import entities.User;

public class UserMapper {
    public static UserDTO mapEntityToDto(User entityUser) {
        if (null == entityUser) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setCustomerId(entityUser.getCustomerId());
        userDTO.setUsername(entityUser.getUsername());
        userDTO.setPassword(entityUser.getPassword());
        userDTO.setFirstName(entityUser.getFirstName());
        userDTO.setLastName(entityUser.getLastName());

        return userDTO;
    }

    public static User mapDtoToEntity(UserDTO userDTO) {
        if (null == userDTO) {
            return null;
        }

        User userEntity = new User();
        userEntity.setCustomerId(userDTO.getCustomerId());
        userEntity.setUsername(userDTO.getUsername());
        userEntity.setPassword(userDTO.getPassword());
        userEntity.setFirstName(userDTO.getFirstName());
        userEntity.setLastName(userDTO.getLastName());

        return userEntity;
    }
}
