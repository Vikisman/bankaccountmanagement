package entities;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;

@Entity
@Table(name = "card", schema = "bank_account_management")
public class Card {

    @Id
    @Column(name = "cardId")
    private Integer cardId;

    @Column(name = "customerId")
    private Integer customerId;


    @Column(name = "cardholderName")
    private String cardholderName;

    @Column(name = "cardExpDate")
    private Integer cardExpDate;

    @Column(name = "cvv")
    private Integer cvv;

    @Column(name = "balance")
    private Integer balance;

    @Column(name = "pin")
    private Integer pin;


    public Card() {
    }

    public Card(Integer customerId, String cardholderName, Integer cardExpDate, Integer cvv,
                Integer balance, Integer pin) {
        this.customerId = customerId;
        this.cardholderName = cardholderName;
        this.cardExpDate = cardExpDate;
        this.cvv = cvv;
        this.balance = balance;
        this.pin = pin;
    }

    public Card(Integer cardId, Integer customerId, String cardholderName, Integer cardExpDate, Integer cvv,
                Integer balance, Integer pin) {
        this.cardId = cardId;
        this.customerId = customerId;
        this.cardholderName = cardholderName;
        this.cardExpDate = cardExpDate;
        this.cvv = cvv;
        this.balance = balance;
        this.pin = pin;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Integer getCardExpDate() {
        return cardExpDate;
    }

    public void setCardExpDate(Integer cardExpDate) {
        this.cardExpDate = cardExpDate;
    }

    public Integer getCvv() {
        return cvv;
    }

    public void setCvv(Integer cvv) {
        this.cvv = cvv;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardId=" + cardId +
                ", customerId=" + customerId +
                ", cardholderName='" + cardholderName + '\'' +
                ", cardExpDate=" + cardExpDate +
                ", cvv=" + cvv +
                ", balance=" + balance +
                ", pin=" + pin +
                '}';
    }
}

