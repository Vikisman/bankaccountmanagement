import config.CardSession;
import config.UserSession;
import dto.CardDTO;
import dto.UserCard;
import dto.UserDTO;
import entities.Card;
import entities.User;
import mapper.CardMapper;
import mapper.UserMapper;
import repository.CardRepository;
import repository.CardRepositoryImpl;
import repository.UserRepository;
import repository.UserRepositoryImpl;

import java.util.*;

public class Application {
    public static void main(String[] args) {
        System.out.println("App starting..." + "\n" + "booting...");
        //enterAccountManagementSystem();
        //MenuDecisionFiveDeleteCard();
        int customerId = 1234;
        CardRepositoryImpl.getInstance().findByCustomerId(customerId);
    }

    public static void enterAccountManagementSystem() {
        int userDecision = getUserDecision();

        if (userDecision == 1) {
            if (isExistingUser()) {
                System.out.println("Logging in... ");
                actionsForSuccessfullyAuthenticatedUser();
            } else {
                enterAccountManagementSystem();
            }
        } else if (userDecision == 2) {
            System.out.println("Creating a new account:");
            createNewAccount();
        }


    }

    private static int getUserDecision() {
        displayMenu();
        List<Integer> loginCondition = Arrays.asList(1, 2);
        return getDecisionByCondition(loginCondition);
    }

    private static int getDecisionByCondition(List<Integer> conditions) {
        Scanner scanner = new Scanner(System.in);
        int userDecision = 0;
        while (!conditions.contains(userDecision)) {
            try {
                userDecision = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Please enter a valid option");
                userDecision = 0;
                scanner.nextLine();
            }
        }
        return userDecision;
    }


    public static void displayMenu() {
        System.out.println("Welcome to your bank account manager.");
        System.out.println("Please type what you want to do:");
        System.out.println("1. Login");
        System.out.println("2. Create new account");

    }

    private static boolean isExistingUser() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter username:");
        String username = scanner.nextLine();
        System.out.println("Enter password");
        String password = scanner.nextLine();

        UserRepository userRepository = UserRepositoryImpl.getInstance();
        Optional<User> optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isPresent()) {
            User foundUser = optionalUser.get();
            return checkPassword(password, foundUser);
        } else {
            System.out.printf("User %s does not exist%n", username);
            System.out.println("Returning back to main menu...");
            return false;
        }

    }

    private static boolean checkPassword(String password, User foundUser) {
        int counter = 0;
        Scanner scanner = new Scanner(System.in);
        while (counter < 3) {
            UserSession userSession = UserSession.getInstance();
            UserDTO userDTO = UserMapper.mapEntityToDto(foundUser);
            if (password.equals(foundUser.getPassword())) {
                System.out.println("Authentication successfully");
                userSession.setUserSessionDTO(userDTO);
                return true;
            } else {
                counter++;
                System.out.println("Wrong password! Please enter the password again.");
                password = scanner.nextLine();
                if (password.equals(foundUser.getPassword())) {
                    System.out.println("Authentication successfully");
                    userSession.setUserSessionDTO(userDTO);
                    return true;
                }
            }
        }
        System.out.println("Incorrect password");
        return false;
    }

    private static int getLoggedUserDecision() {
        List<Integer> loggedInCondition = Arrays.asList(1, 2, 3, 4, 5);
        return getDecisionByCondition(loggedInCondition);
    }


    public static void actionsForSuccessfullyAuthenticatedUser() {
        displayLoggedUserMenu();
        int loggedUserDecision = getLoggedUserDecision();

        switch (loggedUserDecision) {
            case 1:
                MenuDecisionOneCheckBalance();
                break;
            case 2:
                MenuDecisionTwoTransferMoney();
                break;
            case 3:
                MenuDecisionThreeCheckTransactionHistory();
                break;
            case 4:
                MenuDecisionFourAddCard();
                break;
            case 5:
                MenuDecisionFiveDeleteCard();
                break;
            case 0:
                MenuDecisionZeroLogout();
                break;
        }
    }

    private static void displayLoggedUserMenu() {
        System.out.println("Please type what you want to do:");
        System.out.println("1.Check balance");
        System.out.println("2.Transfer");
        System.out.println("3.Check transactions history");
        System.out.println("4.Add card");
        System.out.println("5.Delete card");
        System.out.println("0.Logout");
    }

    private static void createNewAccount() {
        Scanner scanner = new Scanner(System.in);

        User user = new User();

        System.out.println("Enter customer id:");
        user.setCustomerId(scanner.nextInt());

        String clearTheCode = scanner.nextLine();
        System.out.println("Set username:");
        user.setUsername(scanner.nextLine());

        System.out.println("Set password:");
        user.setPassword(scanner.nextLine());

        System.out.println("Enter first name:");
        user.setFirstName(scanner.nextLine());

        System.out.println("Enter last name:");
        user.setLastName(scanner.nextLine());


        UserRepository userRepository = UserRepositoryImpl.getInstance();
        userRepository.create(user);
        System.out.println(user + " has been created");

        enterAccountManagementSystem();
    }

    //TODO Create this method
    private static void MenuDecisionOneCheckBalance() {
    }

    //TODO Create this method
    private static void MenuDecisionTwoTransferMoney() {
    }

    //TODO Create this method
    private static void MenuDecisionThreeCheckTransactionHistory() {
    }

    private static void MenuDecisionFourAddCard() {
        Scanner scanner = new Scanner(System.in);

        Card card = new Card();

        System.out.println("Enter customerId:");
        card.setCustomerId(scanner.nextInt());

        String clearTheCode = scanner.nextLine();
        System.out.println("Enter cardholder name:");
        card.setCardholderName(scanner.nextLine());

        System.out.println("Enter card Id:");
        card.setCardId(scanner.nextInt());

        System.out.println("Enter card expiration year:");
        card.setCardExpDate(scanner.nextInt());

        System.out.println("Enter card cvv:");
        card.setCvv(scanner.nextInt());

        System.out.println("Enter money amount:");
        card.setBalance(scanner.nextInt());

        System.out.println("Enter pin:");
        card.setPin(scanner.nextInt());

        CardRepository cardRepository = CardRepositoryImpl.getInstance();
        cardRepository.create(card);
        System.out.println(card + "has been added");

        actionsForSuccessfullyAuthenticatedUser();
    }

    // this method uses an implementation that finds a card by its ID and deletes it
    //TODO method that shows all cards of a specific user (print);
    //TODO Delete findAll and create findCardByCustomerId;
    private static void MenuDecisionFiveDeleteCard() {
        Scanner scanner = new Scanner(System.in);

        // Here comes findCardByCustomerId

        System.out.println("Enter card id:");
        int cardIdToBeDeleted = scanner.nextInt();

        System.out.println("Are you sure you want to delete the card with Id: " + cardIdToBeDeleted);
        System.out.println("1. YES");
        System.out.println("2. NO");

        int decision = scanner.nextInt();

        if (decision == 1) {
            Optional<Card> foundCard = CardRepositoryImpl.getInstance().findByCardId(cardIdToBeDeleted);

            if (foundCard.isPresent()) {
                if(isExistingCard()) {
                    CardRepositoryImpl.getInstance().delete(foundCard.get());
                    System.out.println("Card has been successfully deleted.");
                }
            } else {
                System.out.println("Card not found. Redirecting to main menu...");
            }
        } else {
            System.out.println("Redirecting to main menu...");
        }
        actionsForSuccessfullyAuthenticatedUser();
    }

    public static boolean isExistingCard() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter card id again:");
        int id = scanner.nextInt();
        System.out.println("Enter card PIN:");
        int pin = scanner.nextInt();
        CardRepository cardRepository = CardRepositoryImpl.getInstance();
        Optional<Card> optionalCard = cardRepository.findByCardId(id);
        if (optionalCard.isPresent()) {
            Card foundCard = optionalCard.get();
            return checkPin(pin, foundCard);
        } else {
            System.out.println("Incorrect PIN!");
            return false;
        }
    }

    private static boolean checkPin(Integer pin, Card foundCard) {
        // checks if found card's pin exists
        int counter = 0;
        Scanner scanner = new Scanner(System.in);
        while (counter < 3) {
            CardSession cardSession = CardSession.getInstance();
            CardDTO cardDTO = CardMapper.mapEntityToDto(foundCard);
            if (pin.equals(foundCard.getPin())) {
                System.out.println("Correct PIN!");
                cardSession.setCardSessionDTO(cardDTO);
                return true;
            } else {
                counter++;
                System.out.println("Incorrect PIN! \n Insert PIN again!");
                pin = scanner.nextInt();
                if (pin.equals(foundCard.getPin())) {
                    System.out.println("Correct PIN!");
                    cardSession.setCardSessionDTO(cardDTO);
                    return true;
                }
            }
        }
        System.out.println("Access denied.");
        return false;
    }

    //TODO Create this method
    private static void MenuDecisionZeroLogout() {
    }

    private static void logoutUser() {
        // todo 1 get user from session
        // todo 2 set user from session user.logged = false
        // todo 3 set user from session = null
        // todo 4 return to main menu
    }
}


