package repository;

import entities.User;

import java.util.Optional;

public interface UserRepository {

    void create(User user);

    void delete(User user);

    void update(User user);

    Optional <User> findByUsername (String username);
}

