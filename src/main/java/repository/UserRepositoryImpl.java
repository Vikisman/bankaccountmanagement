package repository;

import config.SessionManager;
import entities.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

public class UserRepositoryImpl implements UserRepository {
    private static UserRepositoryImpl instance;

    private UserRepositoryImpl() {

    }

    public static UserRepositoryImpl getInstance() {
        if (null == instance) {
            instance = new UserRepositoryImpl();
        }
        return instance;
    }

    @Override
    public void create(User user) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.save(user);

            transaction.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void update(User user) {

    }

    @Override
    public Optional<User> findByUsername(String username) {

        try {
            Session session = SessionManager.getSessionFactory().openSession();
            User user = (User) session.createQuery("from User where username =: username")
                    .setString("username", username)
                    .uniqueResult();
            session.close();
            return Optional.ofNullable(user);

        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
