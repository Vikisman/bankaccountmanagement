package repository;

import entities.Card;

import java.util.List;
import java.util.Optional;

public interface CardRepository {

    void create (Card card);

    void delete (Card card);

    void update (Card card);

    Optional<Card> findByCardId(Integer cardId);

    Optional<Card> findByCardCvv(Integer cardCvv);

    Optional<List<Card>> findByCustomerId(Integer customerId);

    Optional<List<Card>> findAll();
}
