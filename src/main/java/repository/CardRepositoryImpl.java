package repository;

import config.SessionManager;
import entities.Card;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class CardRepositoryImpl implements CardRepository {
    private static CardRepositoryImpl instance;

    private CardRepositoryImpl() {

    }

    public static CardRepositoryImpl getInstance() {
        if (null == instance) {
            instance = new CardRepositoryImpl();
        }
        return instance;
    }


    @Override
    public void create(Card card) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.save(card);

            transaction.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public void delete(Card card) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.delete(card);

            transaction.commit();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();

            if (transaction != null) {
                transaction.rollback();
            }
            if (session != null) {
                session.close();
            }
        }

    }

    @Override
    public void update(Card card) {

    }

    @Override
    public Optional<Card> findByCardId(Integer cardId) {
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            Card card = session.find(Card.class, cardId);
            session.close();
            return Optional.ofNullable(card);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Optional<Card> findByCardCvv(Integer cardCvv) {
        try{
            Session session = SessionManager.getSessionFactory().openSession();
            Card card = session.find(Card.class, cardCvv);
            session.close();
            return Optional.ofNullable(card);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    //Dunno if it works...
    @Override
    public Optional<List<Card>> findByCustomerId(Integer customerId){
        try{
            Session session = SessionManager.getSessionFactory().openSession();
            List<Card> card = session.createQuery("from Card where customerId =: customerId", Card.class).list();
            session.close();
            return Optional.ofNullable(card);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<Card>> findAll() {
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            List<Card> card = session.createQuery("from Card", Card.class).list();
            session.close();
            return Optional.ofNullable(card);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }


}
