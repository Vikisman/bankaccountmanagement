-- Create user table
CREATE TABLE user(
customerId int primary key,
username varchar(50) not null,
password varchar(50) not null,
firstName varchar(50)not null,
lastName varchar(50) not null,
unique(customerId, username)
);

-- Create card table
CREATE TABLE card(
customerId int primary key,
cardholderName varchar(100) not null,
cardId int not null,
cardExpDate int not null,
cvv int not null,
moneyAmount int,
unique(customerId, cardId)
);

 private static void MenuDecisionFiveDeleteCard() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(CardRepositoryImpl.getInstance().findAll());

        System.out.println("Enter card id:");
        int cardIdToBeDeleted = scanner.nextInt();

        System.out.println("Are you sure you want to delete the card with Id: " + cardIdToBeDeleted);
        System.out.println("1. YES");
        System.out.println("2. NO");

        int decision = scanner.nextInt();

        if (decision == 1) {
            Optional<Card> foundCard = CardRepositoryImpl.getInstance().findByCardId(cardIdToBeDeleted);

            if (foundCard.isPresent()) {

                    CardRepositoryImpl.getInstance().delete(foundCard.get());
                    System.out.println("Card has been successfully deleted.");

            } else {
                System.out.println("Card not found. Redirecting to main menu...");
            }
        } else {
            System.out.println("Redirecting to main menu...");
        }
        actionsForSuccessfullyAuthenticatedUser();
    }